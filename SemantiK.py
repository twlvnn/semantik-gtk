#!/usr/bin/python3
import gi
import random
import numpy as np
import base64
import sys, os
import re, json
from gensim.models import KeyedVectors
from gensim.models import FastText
import logging as log

gi.require_version('Gtk', '4.0')
gi.require_version('Pango', '1.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gio, Gdk, Graphene, GLib, GObject, Pango

logger = log.getLogger()
logger.setLevel(log.INFO)
formatter = log.Formatter('%(asctime)s | %(levelname)s | %(message)s', '%d-%m-%Y %H:%M:%S')
# Log inside the terminal
stdout_handler = log.StreamHandler(sys.stdout)
stdout_handler.setFormatter(formatter)
logger.addHandler(stdout_handler)


config_folder = str(GLib.get_user_config_dir()) + "/SemantiK"
if not os.path.exists(config_folder):
    log.info('Creating config folder...')
    os.makedirs(config_folder)


class SemantiK(Adw.Application):
    def __init__(self):
        super().__init__(application_id="net.krafting.SemantiK")
        self.connect("activate", self.on_activate)
    
    def on_activate(self, app):

        # Initialize some variables for use later
        self.all_guesses_variables = {}
        self.all_guesses_order = []
        self.config = {}
        self.latest_game = {}
        self.latest_game['guessed_word'] = []
        self.latest_game['guessed_word_unordered'] = []
        self.animation_timed = ""
        self.current_history_pos = 0
        self.model_file_name_fallback = "/app/models/frWac_non_lem_no_postag_no_phrase_200_cbow_cut100.bin"
        self.random_word_list_fallback = "/app/word_lists/francais.list"
        # self.current_lang = Gtk.get_default_language().to_string()
        self.config_folder = str(GLib.get_user_config_dir()) + "/SemantiK"
        if not os.path.exists(self.config_folder):
            log.info('Creating config folder...')
            os.makedirs(self.config_folder)

        self.load_settings(just_load_value=True)

        try:
            self.custom_model_name = self.config['model_name']
            self.custom_wordlist_name = self.config['wordlist_name']
            log.info("Settings: " + self.custom_model_name + " / " + self.custom_wordlist_name)
        except Exception as e:
            log.error(str(e))
            self.custom_model_name = "custom_model.bin"
            self.custom_wordlist_name = "custom_wordlist.list"
            log.info("Settings: " + self.custom_model_name + " / " + self.custom_wordlist_name)



        # If we have a custom model in the config directory, we load it.
        if os.path.exists(str(self.config_folder) + "/" + str(self.custom_model_name) ) and self.custom_model_name != "":
            self.model_file_name = str(self.config_folder) + "/" + str(self.custom_model_name)
            log.info('Loading custom model file: ' + str(self.config_folder) + "/" + str(self.custom_model_name))
        else:
            self.model_file_name = "/app/models/frWac_non_lem_no_postag_no_phrase_200_cbow_cut100.bin"
            log.info('Loading default model file.')


        # If we have a custom word list in the config directory, we load it.
        if os.path.exists(self.config_folder + "/" + str(self.custom_wordlist_name)) and self.custom_wordlist_name != "":
            self.random_word_list = str(self.config_folder) + "/" + str(self.custom_wordlist_name)
            log.info('Loading custom word file: ' + str(self.config_folder) + "/" + str(self.custom_wordlist_name))
        else:
            self.random_word_list = "/app/word_lists/francais.list"
            log.info('Loading default word file.')



        # If we can't load the word model, we load the fallback one.
        try:
            self.model = KeyedVectors.load_word2vec_format(str(self.model_file_name), binary=True, unicode_errors="ignore")
            log.info('Loaded custom model file.')
        except Exception as e:
            log.error("Cannot load custom model file.") 
            log.error(str(e)) 
            self.model = KeyedVectors.load_word2vec_format(str(self.model_file_name_fallback), binary=True, unicode_errors="ignore")
               

        try:
            self.all_word_list = open(str(self.random_word_list)).read().splitlines()
        except:
            log.error("Cannot load custom wordlist file. Using default") 
            self.all_word_list = open(str(self.random_word_list_fallback)).read().splitlines()

        self.current_word_to_find = random.choice(self.all_word_list)
        
        self.current_word_to_find_code_sharing = base64.b64encode(self.current_word_to_find.encode("utf-8")).decode("ascii") 



        # Set default settings
        self.window = Adw.ApplicationWindow(application=app)
        self.window.set_default_size(850, 600)
        self.window.set_property('height-request', 200)
        self.window.set_property('width-request', 300)
        self.window.set_title("SemantiK")
        GLib.set_application_name("SemantiK")
        self.set_application_id("net.krafting.SemantiK")
        self.window.present()

        # Custom CSS
        cssLevelBar = """
        levelbar > trough > block.filled {
            background-image: linear-gradient(
                90deg,
                rgb(246,211,45),
                rgb(255,120,0),
                rgb(224,27,36)
            );
        }
        .success-action-row-button {
            background: #76d08a;
        }
        .success-action-row-button:hover {
            background: #6bbd7d;
        }
        .success-action-row {
            background: #8ff0a4;
            color: #125B39;
        }"""

        display = Gdk.Display.get_default()

        css_provider = Gtk.CssProvider()
        css_provider.load_from_string(cssLevelBar)
        Gtk.StyleContext.add_provider_for_display(
            display,
            css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION,
        )

        #######################
        # MAIN PAGE
        #######################
        self.Main_boxMain = Adw.ToolbarView()
        # All page is a Toast Overlay to display burnt toast
        self.Main_Toast = Adw.ToastOverlay()
        self.Main_Toast.set_child(self.Main_boxMain)
        self.window.set_content(self.Main_Toast)  # Horizontal box to window


        self.Main_page = Adw.StatusPage()
        self.Main_page.set_title('SemantiK')
        self.Main_page.set_description('Trouver le mot secret.')

        self.Main_clamp = Adw.Clamp()
        self.Main_clamp.set_property('valign', Gtk.Align.CENTER)
        self.Main_page.set_child(self.Main_clamp)



        self.Main_box2 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.Main_clamp.set_child(self.Main_box2)

        # Box with the Success Word Found Action row
        self.Main_listboxFoundWord = Gtk.ListBox()
        self.Main_listboxFoundWord.set_selection_mode(Gtk.SelectionMode.NONE)
        self.Main_listboxFoundWord.add_css_class("boxed-list")
        self.Main_listboxFoundWord.set_visible(False)
        self.Main_listboxFoundWord.add_css_class("success-action-row")
        self.Main_box2.append(self.Main_listboxFoundWord)

        self.Main_Button_FoundWord = Gtk.Button(label="Nouveau Mot")
        self.Main_Button_FoundWord.set_valign(Gtk.Align.CENTER)
        self.Main_Button_FoundWord.add_css_class("success-action-row-button")
        self.Main_Button_FoundWord.connect("clicked", self.new_word)

        self.Main_felicitationsActionRow = Adw.ActionRow()
        self.Main_felicitationsActionRow.set_title("Félicitations !")
        self.Main_felicitationsActionRow.add_suffix(self.Main_Button_FoundWord)
        self.Main_felicitationsActionRow.set_subtitle("Vous avez trouvé le mot en XXX coups.")
        self.Main_listboxFoundWord.append(self.Main_felicitationsActionRow)


        self.Main_listbox = Gtk.ListBox()
        self.Main_listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        self.Main_listbox.add_css_class("boxed-list")
        self.Main_box2.append(self.Main_listbox)

        # Input de l'user
        self.WordGuess_Input = Adw.EntryRow()
        self.WordGuess_Input.set_title('Mot')

        # # Listen for keyboard event in the text box
        evk1 = Gtk.EventControllerKey()
        evk1.connect("key-pressed", self.input_event_handle)

        self.WordGuess_Input.connect("entry-activated", self.word_guess)
        self.WordGuess_Input.add_controller(evk1)
        self.Main_listbox.append(self.WordGuess_Input)


        # Current word guessed
        self.latest_word_guess_levelBar = Gtk.LevelBar()
        self.latest_word_guess_levelBar.set_mode(Gtk.LevelBarMode.CONTINUOUS)
        self.latest_word_guess_levelBar.set_value(0)
        self.latest_word_guess_levelBar.add_css_class("background-level-bar")
        self.latest_word_guess_levelBar.set_max_value(1000)
        self.latest_word_guess_levelBar.set_property('valign', Gtk.Align.CENTER)
        self.latest_word_guess_levelBar.set_property('width-request', 180)
        self.latest_word_guess_levelBar.set_min_value(0)

        self.emojiLabelCurrent = Gtk.Label(label="🟦")

        self.latest_word_guess = Adw.ActionRow()
        self.latest_word_guess.set_use_markup(False)
        self.latest_word_guess.set_title("-")
        # remove ability to focus, so it doesn't take focus when using up and down on the input above
        self.latest_word_guess.set_can_focus(False)
        self.latest_word_guess.set_subtitle("-")
        self.latest_word_guess.add_suffix(self.latest_word_guess_levelBar)
        self.latest_word_guess.add_prefix(self.emojiLabelCurrent)
        self.Main_listbox.append(self.latest_word_guess)

        self.Main_boxMain.set_content(self.Main_page)


        #######################
        # MAIN PAGE HEADERBAR
        #######################
        self.header = Adw.HeaderBar()
        self.Main_boxMain.add_top_bar(self.header)

        self.open_button = Gtk.Button(label="Partager")
        self.header.pack_start(self.open_button)
        self.open_button.set_icon_name("emblem-shared-symbolic")
        self.open_button.connect("clicked", self.show_sharing)

        # Create a popover
        self.menu = Gio.Menu.new()
        self.popover = Gtk.PopoverMenu()  # Create a new popover menu
        self.popover.set_menu_model(self.menu)

        # Create a menu button
        self.hamburger = Gtk.MenuButton()
        self.hamburger.set_popover(self.popover)
        self.hamburger.set_icon_name("open-menu-symbolic")  # Give it a nice icon

        # Add menu button to the header bar (at the end)
        self.header.pack_end(self.hamburger)

        # Request a new word
        action_new_word = Gio.SimpleAction.new("new_word_generate", None)
        action_new_word.connect("activate", self.new_word)
        self.window.add_action(action_new_word) 

        action_how_to_play = Gio.SimpleAction.new("show_how_to_play", None)
        action_how_to_play.connect("activate", self.show_how_to_play)
        self.window.add_action(action_how_to_play) 

        # Indices
        action_show_indice = Gio.SimpleAction.new("show_indice", None)
        action_show_indice.connect("activate", self.show_indice)
        self.window.add_action(action_show_indice) 


        # Add preference dialog
        action_preferences = Gio.SimpleAction.new("preferences", None)
        action_preferences.connect("activate", self.show_settings)
        self.window.add_action(action_preferences) 

        # Add shortcuts dialog
        action_shortcuts = Gio.SimpleAction.new("shortcuts", None)
        action_shortcuts.connect("activate", self.show_shortcuts)
        self.window.add_action(action_shortcuts) 

        # Add an about dialog
        action_about = Gio.SimpleAction.new("about", None)
        action_about.connect("activate", self.show_about)
        self.window.add_action(action_about) 

        # We add everything and their action to the menu.
        self.menu.append("Obtenir un indice", "win.show_indice")
        self.menu.append("Nouveau Mot", "win.new_word_generate")

        self.menu_section_1 = Gio.Menu.new()
        self.menu_section_1.append("Comment Jouer ?", "win.show_how_to_play")
        self.menu_section_1.append("Preferences", "win.preferences")
        self.menu.insert_section(3, None, self.menu_section_1)

        self.menu_section_2 = Gio.Menu.new()
        self.menu_section_2.append("Raccourcis Clavier", "win.shortcuts")
        self.menu_section_2.append("À Propos", "win.about")
        self.menu.insert_section(3, None, self.menu_section_2)

        evk = Gtk.EventControllerKey.new()
        evk.connect("key-pressed", self.key_press)
        self.window.add_controller(evk)


        #######################
        # SHARING DIALOG
        #######################
        self.ShareWord_application_dialog = Adw.Dialog()
        self.ShareWord_application_dialog.set_title("Partager")
        self.ShareWord_application_dialog.set_content_width(600)
        self.ShareWord_boxMain = Adw.ToolbarView()

        # New stream Header bar
        self.ShareWord_header = Adw.HeaderBar()
        self.ShareWord_boxMain.add_top_bar(self.ShareWord_header)

        self.record_button = Gtk.Button(label="OK")
        self.ShareWord_header.pack_end(self.record_button)
        self.record_button.set_icon_name("plus-large-symbolic")
        self.record_button.set_label("OK")
        self.record_button.add_css_class("suggested-action")
        self.record_button.connect("clicked", self.ok_sharing)

        # Set window child
        self.ShareWord_application_dialog.set_child(self.ShareWord_boxMain)

        self.ShareWord_page = Adw.StatusPage()
        self.ShareWord_page.set_title('Partager')
        self.ShareWord_page.set_description('Partagez votre mot actuel avec des amis. Ou importez le mot d\'un ami.')

        self.ShareWord_clamp = Adw.Clamp()
        self.ShareWord_page.set_child(self.ShareWord_clamp)

        self.ShareWord_box2 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.ShareWord_clamp.set_child(self.ShareWord_box2)


        self.ShareWord_listbox = Gtk.ListBox()
        self.ShareWord_listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        self.ShareWord_box2.append(self.ShareWord_listbox)
        self.ShareWord_listbox.add_css_class("boxed-list")

        self.ShareWord_actionrowButton = Gtk.Button(label="Copy")
        self.ShareWord_actionrowButton.set_valign(Gtk.Align.CENTER)
        self.ShareWord_actionrowButton.connect("clicked", self.copy_stuff, self.current_word_to_find_code_sharing)

        self.ShareWord_actionrow = Adw.ActionRow()
        self.ShareWord_actionrow.set_title('Votre Code de Mot')
        self.ShareWord_actionrow.add_suffix(self.ShareWord_actionrowButton)
        self.ShareWord_actionrow.set_subtitle(self.current_word_to_find_code_sharing)
        self.ShareWord_listbox.append(self.ShareWord_actionrow)

        self.LabelSharing = Gtk.Label(label="Importer")
        self.LabelSharing.add_css_class("title-3")
        self.LabelSharing.set_margin_bottom(15)
        self.LabelSharing.set_margin_top(15)
        self.ShareWord_box2.append(self.LabelSharing)

        self.ImportWord_listbox = Gtk.ListBox()
        self.ImportWord_listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        self.ShareWord_box2.append(self.ImportWord_listbox)
        self.ImportWord_listbox.add_css_class("boxed-list")

        self.ImportWord_entryrow = Adw.EntryRow()
        self.ImportWord_entryrow.set_title('Importer un Code de Mot.')
        self.ImportWord_listbox.append(self.ImportWord_entryrow)

        # Add the page to the window
        self.ShareWord_boxMain.set_content(self.ShareWord_page)



        #######################
        # INDICE PAGE
        #######################
        self.IndicePage_application_dialog = Adw.Dialog()
        self.IndicePage_application_dialog.set_title("Indice")
        self.IndicePage_application_dialog.set_content_width(620)
        self.IndicePage_boxMain = Adw.ToolbarView()

        # New stream Header bar
        self.IndicePage_header = Adw.HeaderBar()
        self.IndicePage_boxMain.add_top_bar(self.IndicePage_header)

        # Set window child
        self.IndicePage_application_dialog.set_child(self.IndicePage_boxMain)

        self.IndicePage_page = Adw.StatusPage()
        self.IndicePage_page.set_title('Un Indice')
        self.IndicePage_page.set_description('Un des 1200 mots les plus proches du mot secret.')

        self.IndicePage_clamp = Adw.Clamp()
        self.IndicePage_page.set_child(self.IndicePage_clamp)
        self.IndicePage_box2 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.IndicePage_clamp.set_child(self.IndicePage_box2)

        self.IndicePage_listbox = Gtk.ListBox()
        self.IndicePage_listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        self.IndicePage_box2.append(self.IndicePage_listbox)
        self.IndicePage_listbox.add_css_class("boxed-list")

        self.IndicePage_actionrowButton = Gtk.Button(label="Copy")
        self.IndicePage_actionrowButton.set_valign(Gtk.Align.CENTER)

        self.IndiceEmoji = Gtk.Label(label='🌟')
        
        self.IndicePage_actionrow = Adw.ActionRow()
        self.IndicePage_actionrow.set_title('Votre indice:')
        self.IndicePage_actionrow.add_prefix(self.IndiceEmoji)
        self.IndicePage_actionrow.add_suffix(self.IndicePage_actionrowButton)
        self.IndicePage_listbox.append(self.IndicePage_actionrow)

        # Add the page to the window
        self.IndicePage_boxMain.set_content(self.IndicePage_page)




        #######################
        # How to play & Meaning
        #######################
        self.HowToPlayMeaning_application_dialog = Adw.Dialog()
        self.HowToPlayMeaning_application_dialog.set_title("Comment Jouer")
        self.HowToPlayMeaning_application_dialog.set_content_width(650)
        
        self.HowToPlayMeaning_boxMain = Adw.ToolbarView()

        # New stream Header bar
        self.HowToPlayMeaning_header = Adw.HeaderBar()
        self.HowToPlayMeaning_boxMain.add_top_bar(self.HowToPlayMeaning_header)

        # Set window child
        self.HowToPlayMeaning_application_dialog.set_child(self.HowToPlayMeaning_boxMain)

        self.HowToPlayMeaning_page = Adw.StatusPage()
        self.HowToPlayMeaning_page.set_title('Comment Jouer')
        self.HowToPlayMeaning_page.set_description('Le but du jeu est de trouver le mot secret en essayant de s\'en approcher le plus possible contextuellement. Chaque mot se voit attribuer une température dont des valeurs intéressantes sont données en légende ci-dessous. Si votre mot se trouve dans les 1000 mots les plus proches, un indice de progression gradué de 1 à 1000 ‰ apparaîtra.')

        self.HowToPlayMeaning_clamp = Adw.Clamp()
        self.HowToPlayMeaning_page.set_child(self.HowToPlayMeaning_clamp)

        self.HowToPlayMeaning_box2 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.HowToPlayMeaning_clamp.set_child(self.HowToPlayMeaning_box2)

        self.HowToPlayMeaning_listbox = Gtk.ListBox()
        self.HowToPlayMeaning_listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        self.HowToPlayMeaning_box2.append(self.HowToPlayMeaning_listbox)
        self.HowToPlayMeaning_listbox.add_css_class("boxed-list")

        self.HowToPlayMeaning_actionrow0 = Adw.ActionRow()
        self.HowToPlayMeaning_actionrow0.set_title('1000 ‰')
        self.HowToPlayMeaning_actionrow0.set_subtitle("100 °C")

        self.HowToPlayMeaning_actionrow1 = Adw.ActionRow()
        self.HowToPlayMeaning_actionrow1.set_title('999 ‰')
        self.HowToPlayMeaning_actionrow1.set_subtitle("")

        self.HowToPlayMeaning_actionrow10 = Adw.ActionRow()
        self.HowToPlayMeaning_actionrow10.set_title('990 ‰')
        self.HowToPlayMeaning_actionrow10.set_subtitle("")

        self.HowToPlayMeaning_actionrow100 = Adw.ActionRow()
        self.HowToPlayMeaning_actionrow100.set_title('900 ‰')
        self.HowToPlayMeaning_actionrow100.set_subtitle("")

        self.HowToPlayMeaning_actionrow1000 = Adw.ActionRow()
        self.HowToPlayMeaning_actionrow1000.set_title('1 ‰')
        self.HowToPlayMeaning_actionrow1000.set_subtitle("")

        self.emojiLabelRules0 = Gtk.Label(label="")
        self.emojiLabelRules1 = Gtk.Label(label="")
        self.emojiLabelRules10 = Gtk.Label(label="")
        self.emojiLabelRules100 = Gtk.Label(label="")
        self.emojiLabelRules1000 = Gtk.Label(label="")
        self.HowToPlayMeaning_actionrow1000.add_prefix(self.emojiLabelRules1000)
        self.HowToPlayMeaning_actionrow100.add_prefix(self.emojiLabelRules100)
        self.HowToPlayMeaning_actionrow10.add_prefix(self.emojiLabelRules10)
        self.HowToPlayMeaning_actionrow1.add_prefix(self.emojiLabelRules1)
        self.HowToPlayMeaning_actionrow0.add_prefix(self.emojiLabelRules0)

        self.HowToPlayMeaning_listbox.append(self.HowToPlayMeaning_actionrow0)
        self.HowToPlayMeaning_listbox.append(self.HowToPlayMeaning_actionrow1)
        self.HowToPlayMeaning_listbox.append(self.HowToPlayMeaning_actionrow10)
        self.HowToPlayMeaning_listbox.append(self.HowToPlayMeaning_actionrow100)
        self.HowToPlayMeaning_listbox.append(self.HowToPlayMeaning_actionrow1000)



        # Add the page to the window
        self.HowToPlayMeaning_boxMain.set_content(self.HowToPlayMeaning_page)


        #######################
        # SHORTCUTS DIALOG
        #######################
        self.shortcutwindow = Gtk.ShortcutsWindow()
        self.shortcutwindow.set_property('modal', 1)
        self.shortcutwindow.set_transient_for(self.get_active_window())
        self.shortcutwindow.set_hide_on_close(True)

        self.shortcutwindowSection = Gtk.ShortcutsSection()

        self.shortcutwindowGroup = Gtk.ShortcutsGroup()
        self.shortcutwindowGroup.set_property("title", "Global Shortcuts")
        
        # Shortcut CTRL N - Open the sharing window
        self.shortcutShareWord = Gtk.ShortcutsShortcut()
        self.shortcutShareWord.set_property("accelerator", "<ctl>n")
        self.shortcutShareWord.set_property("title", "Ouvrir la fenêtre de partage")
        self.shortcutwindowGroup.add_shortcut(self.shortcutShareWord)

        # Shortcut CTRL I - Ask for a indice
        self.shortcutShareWord = Gtk.ShortcutsShortcut()
        self.shortcutShareWord.set_property("accelerator", "<ctl>i")
        self.shortcutShareWord.set_property("title", "Demander un indice")
        self.shortcutwindowGroup.add_shortcut(self.shortcutShareWord)
        
        # Shortcut CTRL Q - Quit
        self.shortcutQuit = Gtk.ShortcutsShortcut()
        self.shortcutQuit.set_property("accelerator", "<ctl>q")
        self.shortcutQuit.set_property("title", "Quitter")
        self.shortcutwindowGroup.add_shortcut(self.shortcutQuit)

        # Shortcut CTRL ? - Show shortcuts
        self.shortcutKBShortcuts = Gtk.ShortcutsShortcut()
        self.shortcutKBShortcuts.set_property("accelerator", "<ctl>question")
        self.shortcutKBShortcuts.set_property("title", "Raccourcis Clavier")
        self.shortcutwindowGroup.add_shortcut(self.shortcutKBShortcuts)

        # Shortcut CTRL , - Show preferences
        self.shortcutPreferences = Gtk.ShortcutsShortcut()
        self.shortcutPreferences.set_property("accelerator", "<ctl>comma")
        self.shortcutPreferences.set_property("title", "Preferences")
        self.shortcutwindowGroup.add_shortcut(self.shortcutPreferences)

        # Add Groups to section
        self.shortcutwindowSection.add_group(self.shortcutwindowGroup)
        # Add section to Window
        self.shortcutwindow.add_section(self.shortcutwindowSection)


        #######################
        # SETTINGS DIALOG
        #######################
        self.Preferences_application_dialog = Adw.PreferencesDialog()
        self.Preferences_application_dialog.set_title("Préférences")

        # Premiere page des paramètres
        self.Preferences_PreferencePageSoftware = Adw.PreferencesPage()
        self.Preferences_PreferencePageSoftware.set_title('Software')
        self.Preferences_PreferencePageSoftware.set_icon_name('emblem-system-symbolic')
        self.Preferences_application_dialog.add(self.Preferences_PreferencePageSoftware)

        # 1er groupe de préférences
        self.Preferences_Group = Adw.PreferencesGroup()
        self.Preferences_Group.set_title('Préférences SemantiK')
        self.Preferences_Group.set_description('Changez le modèle ou la liste de mot par vos propres données. \nDéposez les fichiers dans ~/.var/app/net.krafting.SemantiK/config/SemantiK/ puis insérer le nom des fichiers ici.\n\nLa modification de ces paramètres nécéssitent un redémarrage de l\'application et de générer un nouveau mot.')
        self.Preferences_PreferencePageSoftware.add(self.Preferences_Group)

        self.Preferences_ModelName = Adw.EntryRow()
        self.Preferences_ModelName.set_title('Modèle de langue personalisé.')
        self.Preferences_Group.add(self.Preferences_ModelName)
        self.Preferences_ModelName.connect("notify", self.save_settings)

        self.Preferences_WordListName = Adw.EntryRow()
        self.Preferences_WordListName.set_title('Liste de mot personalisée.')
        self.Preferences_Group.add(self.Preferences_WordListName)
        self.Preferences_WordListName.connect("notify", self.save_settings)

        self.load_settings()
        self.load_last_word()
        self.most_similar_1 = self.find_pourmillage(1)
        self.most_similar_10 = self.find_pourmillage(10)
        self.most_similar_100 = self.find_pourmillage(100)
        self.most_similar_1000 = self.find_pourmillage(1000)
        self.update_similarity_page()
        
        self.add_to_save_last_word(word_to_guess=self.current_word_to_find)

    def add_to_save_last_word(self, word_to_guess = "", guessed_word = "", reset = False) -> None:
        """
        Function to add a word to a save file. Used to load the 
        """
        if word_to_guess != "":
            self.latest_game['word_to_guess'] = base64.b64encode(word_to_guess.encode("utf-8")).decode("ascii")
        elif reset == True:
            self.latest_game['guessed_word'] = []
        else:
            self.latest_game['guessed_word'] = self.all_guesses_order

        json.dump( self.latest_game, open( self.config_folder + "/latest_game.json", 'w' ) )

    def load_last_word(self) -> None:
        """
        Function to load the settings data at startup of the application
        This is a bit hacky, when you load a value, it send the signal to save settings and overwriting 
        everything else, so we store the config in variable, change the values we want and then load it again
        """
        if not os.path.exists( self.config_folder + "/latest_game.json" ): 
            return False
            

        try:
            # Load the latest game data and decode the word to guess
            self.latest_game = json.load( open( self.config_folder + "/latest_game.json" ) )
            
            if len(self.latest_game["guessed_word_unordered"]) <= 0: 
                log.info('No word were guessed when the app was lastly used. Generating a new word...')
                return False
            self.current_word_to_find = str(base64.b64decode(self.latest_game['word_to_guess'].encode("ascii")).decode("utf-8"))
            
            # load most_similar values, or else they are not loaded 
            self.most_similar_1 = self.find_pourmillage(1)
            self.most_similar_10 = self.find_pourmillage(10)
            self.most_similar_100 = self.find_pourmillage(100)
            self.most_similar_1000 = self.find_pourmillage(1000)

            # Update the Word Code
            self.ShareWord_actionrow.set_subtitle(self.latest_game['word_to_guess'])
            self.ShareWord_actionrowButton.connect("clicked", self.copy_stuff, self.latest_game['word_to_guess'])

            for word in self.latest_game['guessed_word'][::-1]:
                self.word_guess(word_passed=word["word"])
            return True
        except Exception as e:
            log.error(e)
            log.error('Cannot load latest game data')
            return False

    def input_event_handle(self, controller = "", keyval = "", keycode = "", state = ""):
        """
        Function when using modifiers key in the input text box, used to show history of words
        """
        if keyval == Gdk.KEY_Up or keyval == Gdk.KEY_KP_Up:
            self.current_history_pos = self.current_history_pos - 1
            if self.current_history_pos < - len(self.latest_game['guessed_word_unordered']):
                self.current_history_pos = - len(self.latest_game['guessed_word_unordered'])
                return
            try:
                self.WordGuess_Input.set_text(self.latest_game['guessed_word_unordered'][self.current_history_pos])
                # Set the caret position
                num_char_input = list(self.WordGuess_Input.get_text())
                self.WordGuess_Input.set_position(len(num_char_input))
                
            except:
                log.error("Could not load history (ARROW UP)")


        if keyval == Gdk.KEY_Down or keyval == Gdk.KEY_KP_Down:
            self.current_history_pos = self.current_history_pos + 1
            if self.current_history_pos >= 0:
                self.current_history_pos = 0
                self.WordGuess_Input.set_text("")
                return
            try:
                self.WordGuess_Input.set_text(self.latest_game['guessed_word_unordered'][self.current_history_pos])
                # Set the caret position
                num_char_input = list(self.WordGuess_Input.get_text())
                self.WordGuess_Input.set_position(len(num_char_input))
            except:
                log.error("Could not load history (ARROW DOWN)")


    def save_settings(self, action, param) -> None:
        """
        Function to save settings data to the self.config variable
        Then, write a settings.json file to the config folder of the app
        """
        self.config['model_name'] = self.Preferences_ModelName.get_text()
        self.config['wordlist_name'] = self.Preferences_WordListName.get_text()
        json.dump( self.config, open( self.config_folder + "/settings.json", 'w' ) )

    def load_settings(self, just_load_value = False) -> None:
        """
        Function to load the settings data at startup of the application
        This is a bit hacky, when you load a value, it send the signal to save settings and overwriting 
        everything else, so we store the config in variable, change the values we want and then load it again
        """
        if not os.path.exists( str(self.config_folder) + "/settings.json" ): 
            return
        if just_load_value:
            loading_config = json.load( open( str(self.config_folder) + "/settings.json" ) )
            self.config = loading_config
            log.info('Loaded first run settings.')
            return
        try:
            loading_config = json.load( open( str(self.config_folder) + "/settings.json" ) )
            self.Preferences_ModelName.set_text(loading_config['model_name'])
            self.Preferences_WordListName.set_text(loading_config['wordlist_name'])
            self.config = loading_config
            log.info('Loaded modified settings.')
        except:
            log.warning("Error in config file... Using default values")

    def is_valid_word(self, word):
        """
        Function to verify if it's a valid word
        """
        if word == "":
            return False
        elif "!" in word:
            return False
        else:
            return True

    def sort_guess_array(self, array = []):
        """
        Function to sort the array of all the guess to sort them correclty and then get the correct values
        """
        new_array = sorted(array, key=lambda x: x['score'], reverse=True)
        return new_array

    def get_score_emoji(self, score):
        """
        Function to get the emoji from a given word score
        """
        emoji = "🟥"
        if score < 0:
            emoji = "🧊"
        if score >= 0:
            emoji = "🥶"
        if score >= self.most_similar_1000 * 100:
            emoji = "😎"
        if score >= self.most_similar_100 * 100:
            emoji = "🥵"
        if score >= self.most_similar_10 * 100:
            emoji = "🔥"
        if score >= self.most_similar_1 * 100:
            emoji = "😱"
        if score >= 100:
            emoji = "🥳"

        if score <= -999:
            emoji = "🟦"

        return emoji

    def rearange_guessed_word(self, new_array = [], wordGuessedItem = ""):
        """
        Function to rearrange all guessed word in the UI
        """
        for value in new_array:
            current_item_actionrow = self.all_guesses_variables[str(value['word']) + "Main_ActionRowImported"]
            log.debug("reorganizing: " + str(value['word']) + " " + str(value['score']))
            self.Main_listboxResults.remove(current_item_actionrow)
            self.Main_listboxResults.append(current_item_actionrow)
        return new_array

    def word_guess(self, event = "", word_passed = ""):
        """
        Function used when sending a word via the main Input
        """
        # reset the history when sending a word
        self.current_history_pos = 0
        # We remove all special chars from the current string
        # We can add a word to the word list if word_passed is not empty
        if word_passed == "":
            word_guess_user = re.sub(r"[^a-zA-ZÀ-ÿ-]+", "", self.WordGuess_Input.get_text()).lower()
            self.latest_game['guessed_word_unordered'].append(str(word_guess_user))
        else:
            word_guess_user = re.sub(r"[^a-zA-ZÀ-ÿ-]+", "", word_passed).lower()

        score = self.get_word_score(word_guess_user)

        pourmillage = self.get_pourmille_from_score(score)

        # Don't put negative number in the level bar
        if pourmillage < 1:
            pourmillageLevelBar = 0
        else:
            pourmillageLevelBar = pourmillage
        
        # If the word doesn't exist..
        if score == False or not self.is_valid_word(word_guess_user):
            self.WordGuess_Input.set_text("")
            self.latest_word_guess_levelBar.set_value(0)
            self.latest_word_guess.set_title(str(word_guess_user))
            self.emojiLabelCurrent.set_label("🟥")
            self.latest_word_guess.set_subtitle("Je ne connais pas ce mot.")
            return
            

        log.debug(str(word_guess_user))

        if not hasattr(self, 'Main_listboxResults'):
            self.LabelResults = Gtk.Label(label="Results")
            self.LabelResults.add_css_class("title-3")
            self.LabelResults.set_margin_bottom(15)
            self.LabelResults.set_margin_top(15)
            self.LabelResults.set_property('valign', Gtk.Align.CENTER)
            self.Main_box2.append(self.LabelResults)

            # Boite contenant les résultats
            self.Main_listboxResults = Gtk.ListBox()
            self.Main_listboxResults.set_selection_mode(Gtk.SelectionMode.NONE)
            self.Main_listboxResults.add_css_class("boxed-list")
            self.Main_box2.append(self.Main_listboxResults)

        self.emojiLabel = Gtk.Label(label=str(self.get_score_emoji(score)))

        # Update the current word box
        self.latest_word_guess_levelBar.set_value(0)
        self.latest_word_guess_levelBar.set_value(pourmillageLevelBar)
        self.latest_word_guess.set_title(str(word_guess_user))
        self.emojiLabelCurrent.set_label(str(self.get_score_emoji(score)))
        self.latest_word_guess.set_subtitle(str(score) + " °C - " + str(pourmillage) + " ‰")

        # On reset l'animation, utile quand on écrit des mot rapidement
        if self.animation_timed != "":
            self.animation_timed.reset()
        target_timed = Adw.PropertyAnimationTarget.new(self.latest_word_guess_levelBar, "value");
        self.animation_timed = Adw.TimedAnimation.new(self.latest_word_guess_levelBar, 0, pourmillageLevelBar, 2000, target_timed)
        self.animation_timed.connect("done", self.is_word_found, pourmillage)
        self.animation_timed.play()

        if str(word_guess_user) + "levelBarResult" in self.all_guesses_variables:
            self.WordGuess_Input.set_text("")
            return


        self.all_guesses_variables[word_guess_user + "levelBarResult"] = Gtk.LevelBar()
        self.all_guesses_variables[word_guess_user + "levelBarResult"].set_mode(Gtk.LevelBarMode.CONTINUOUS)
        self.all_guesses_variables[word_guess_user + "levelBarResult"].set_value(pourmillageLevelBar)
        self.all_guesses_variables[word_guess_user + "levelBarResult"].set_max_value(1000)
        self.all_guesses_variables[word_guess_user + "levelBarResult"].set_property('valign', Gtk.Align.CENTER)
        self.all_guesses_variables[word_guess_user + "levelBarResult"].set_property('width-request', 180)
        self.all_guesses_variables[word_guess_user + "levelBarResult"].set_min_value(0)

        self.all_guesses_order.append({"word": str(word_guess_user), "score": float(score)})

        # Override current array with a sorted one.
        self.all_guesses_order = self.sort_guess_array(self.all_guesses_order)

        self.all_guesses_variables[word_guess_user + "Main_ActionRowImported"] = Adw.ActionRow()
        self.all_guesses_variables[word_guess_user + "Main_ActionRowImported"].set_use_markup(False)
        self.all_guesses_variables[word_guess_user + "Main_ActionRowImported"].set_title(str(word_guess_user))
        self.all_guesses_variables[word_guess_user + "Main_ActionRowImported"].add_suffix(self.all_guesses_variables[word_guess_user + "levelBarResult"])
        self.all_guesses_variables[word_guess_user + "Main_ActionRowImported"].add_prefix(self.emojiLabel)
        self.all_guesses_variables[word_guess_user + "Main_ActionRowImported"].set_subtitle(str(score) + " °C - " + str(pourmillage) + " ‰")
        self.Main_listboxResults.append(self.all_guesses_variables[word_guess_user + "Main_ActionRowImported"])

        self.rearange_guessed_word(self.all_guesses_order, self.all_guesses_variables[word_guess_user + "Main_ActionRowImported"])

        # Animation for the guessed word (right panel)
        target_timed = Adw.PropertyAnimationTarget.new(self.all_guesses_variables[word_guess_user + "levelBarResult"], "value");
        animation_timed_word = Adw.TimedAnimation.new(self.all_guesses_variables[word_guess_user + "levelBarResult"], 0, pourmillageLevelBar, 2000, target_timed)
        animation_timed_word.play()

        # If we write a new word in the text box, we save it to the save file
        if word_passed == "":
            self.add_to_save_last_word(guessed_word=word_guess_user)

        # Reset the input after entering a word
        self.WordGuess_Input.set_text("")
        

    def is_word_found(self, event, pourmillage):
        """
        Function called after each animation, to show the congrutation box after the animation is played
        """
        if pourmillage >= 1000:
            nombre_de_coup = len(self.all_guesses_order)
            # Show the box
            self.Main_listboxFoundWord.set_visible(True)
            self.Main_felicitationsActionRow.set_subtitle("Vous avez trouvé le mot en " + str(nombre_de_coup) + " coups.")

    def find_pourmillage(self, similarity = 0):
        """
        Function to find the x most similar word and its score.
        """
        score = self.model.most_similar(self.current_word_to_find, topn=similarity)
        return score[-1][1]

    def update_similarity_page(self):
        """
        Function to update the how to play page with the current similarity values
        """
        self.HowToPlayMeaning_actionrow1.set_subtitle(str("{:.2f}".format(self.most_similar_1 * 100)) + " °C")
        self.HowToPlayMeaning_actionrow10.set_subtitle(str("{:.2f}".format(self.most_similar_10 * 100)) + " °C")
        self.HowToPlayMeaning_actionrow100.set_subtitle(str("{:.2f}".format(self.most_similar_100 * 100)) + " °C")
        self.HowToPlayMeaning_actionrow1000.set_subtitle(str("{:.2f}".format(self.most_similar_1000 * 100)) + " °C")

        self.emojiLabelRules0.set_label(str(self.get_score_emoji(100)))
        self.emojiLabelRules1.set_label(str(self.get_score_emoji(self.most_similar_1 * 100)))
        self.emojiLabelRules10.set_label(str(self.get_score_emoji(self.most_similar_10 * 100)))
        self.emojiLabelRules100.set_label(str(self.get_score_emoji(self.most_similar_100 * 100)))
        self.emojiLabelRules1000.set_label(str(self.get_score_emoji(self.most_similar_1000 * 100)))

    def reset_all_with_new_word(self):
        """
        Function to reset forms and put everything back to default (used when asking a new word)
        """
        # Reset the current found word actionrow
        self.latest_word_guess.set_title("-")
        self.latest_word_guess.set_subtitle("-")
        self.emojiLabelCurrent.set_label(str(self.get_score_emoji(-1000)))
        self.latest_word_guess_levelBar.set_value(0)

        # Remove all guessed words
        self.all_guesses_variables = {}
        self.all_guesses_order = []
        self.animation_timed = ""
        self.current_history_pos = 0
        self.latest_game = {}
        self.latest_game['guessed_word'] = []
        self.latest_game['guessed_word_unordered'] = []
        self.add_to_save_last_word(reset=True)

        # Remove all guessed words actionrows
        if hasattr(self, 'LabelResults'):
            self.Main_listboxResults.remove_all()
            self.Main_box2.remove(self.Main_listboxResults)
            self.Main_box2.remove(self.LabelResults)
            del self.Main_listboxResults
            del self.LabelResults

        # Hide the congratz box
        self.Main_listboxFoundWord.set_visible(False)

        # Fix the Copy button
        self.ShareWord_actionrowButton.connect("clicked", self.copy_stuff, self.current_word_to_find_code_sharing)

    def show_indice(self, event = "", event2 = ""):
        """
        Function to get and show an indice of the current word
        """
        incide_list = self.model.most_similar(self.current_word_to_find, topn=1200)
        indice_choosen = random.choice(incide_list)

        self.IndicePage_actionrow.set_title(str(indice_choosen[0]))
        self.IndicePage_actionrow.set_subtitle(str("{:.2f}".format(indice_choosen[1] * 100)))
        self.IndicePage_actionrowButton.connect("clicked", self.copy_stuff, str(indice_choosen[0]))

        self.IndicePage_application_dialog.present(self.get_active_window())

    def new_word(self, event = "", other_event = "", word = "", code = "", list_words = []):
        """
        Function to apply a new word, from the sharing page
        """
        # If we give the function a list
        if len(list_words) > 0:
            list_to_choose_word = list_words
        else:
            list_to_choose_word = self.all_word_list

                

        if code != "":
            self.current_word_to_find = str(base64.b64decode(code.encode("ascii")).decode("utf-8"))
        elif word != "":
            self.current_word_to_find = str(word)
        else:
            self.current_word_to_find = str(random.choice(list_to_choose_word))

        self.most_similar_1 = self.find_pourmillage(1)
        self.most_similar_10 = self.find_pourmillage(10)
        self.most_similar_100 = self.find_pourmillage(100)
        self.most_similar_1000 = self.find_pourmillage(1000)

        self.update_similarity_page()

        # Update the share Word Code
        self.current_word_to_find_code_sharing = base64.b64encode(self.current_word_to_find.encode("utf-8")).decode("ascii") 
        self.ShareWord_actionrow.set_subtitle(self.current_word_to_find_code_sharing)
        
        self.reset_all_with_new_word()

        self.add_to_save_last_word(word_to_guess=self.current_word_to_find)

        # If we ask for a new word
        if event != "":
            self.add_toast("Le nouveau mot a été appliqué. Amusez-vous bien!")

    def get_pourmille_from_score(self, score):
        """
        Function to get the %0 from the score. 
        """
        points_x = [-100, 0, float(self.most_similar_1000) * 100, float(self.most_similar_100) * 100, float(self.most_similar_10) * 100, float(self.most_similar_1) * 100, 100]
        points_y = [-100, 0, 1, 900, 990, 999, 1000]

        pourmillage = np.interp(score, points_x, points_y)

        if pourmillage < 1:
            pourmillage = 0

        return int(pourmillage)


    def copy_stuff(self, event, data):
        """
        Function to copy stuff in the clipboard
        """
        self.clipboard = Gdk.Display().get_default().get_clipboard()
        data_value = Gdk.ContentProvider().new_for_value(value=data)
        self.clipboard.set_content(data_value)

    def ok_sharing(self, event):
        """
        Function when the sharing window is updated, also change the word with the code
        """
        import_word_text = self.ImportWord_entryrow.get_text()
        if import_word_text != "":
            try:
                # Ask a new word (and reset everything)
                self.new_word(code=import_word_text)

                # Show a toast
                self.add_toast("Le nouveau mot a été appliqué. Amusez-vous bien!")

                # Reset the Sharing word box
                self.ImportWord_entryrow.set_text("")
                
            except Exception as e:
                log.error(e)
                log.warning('Cannot decode Word Code.')        
        self.ShareWord_application_dialog.close()

    def show_shortcuts(self, action = None, param = None) -> None:
        """
        Function to present (show) the Shortcuts window
        """
        self.shortcutwindow.present()

    def show_sharing(self, action = None) -> None:
        """
        Function to present (show) the New Recording window
        """
        self.ShareWord_application_dialog.present(self.get_active_window())

    def show_settings(self, action = None, param = None) -> None:
        """
        Function to present (show) the Settings/Preferences window
        """
        self.Preferences_application_dialog.present(self.get_active_window())

    def show_how_to_play(self, action = None, param = None) -> None:
        """
        Function to show the help and how to play
        """
        self.HowToPlayMeaning_application_dialog.present(self.get_active_window())

    def key_press(self, controller, keyval, keycode, state, is_main_window = True) -> None:
        """
        Function to do stuff when keyboard input is pressed.
        Used mostly for Shortcuts
        """
        log.debug("Key pressed: " + str(keyval))

        modifiers = state & Gtk.accelerator_get_default_mod_mask()
        control_mask = Gdk.ModifierType.CONTROL_MASK
        shift_mask = Gdk.ModifierType.SHIFT_MASK
        alt_mask = Gdk.ModifierType.ALT_MASK
        shift_ctrl_mask = control_mask | shift_mask

        # Ctrl+<KEY>
        if control_mask == modifiers:
            # Show new record window on Ctrl + N
            if keyval == Gdk.KEY_n:
                self.show_sharing()
            if keyval == Gdk.KEY_q:
                self.window.close()
            if keyval == Gdk.KEY_i:
                self.show_indice()
            if keyval == Gdk.KEY_comma:
                self.show_settings()
            if keyval == Gdk.KEY_question:
                self.show_shortcuts()
        # Ctrl+Shift+<KEY>
        elif modifiers == shift_ctrl_mask:
            # Duplicate for keyboards that needs to press SHIFT to type the character
            if keyval == Gdk.KEY_question:
                self.show_shortcuts()
        # Alt+<KEY>
        elif modifiers == alt_mask:
            if keyval == Gdk.KEY_A:
                print('something')
        # No modifier
        else:
            if keyval == Gdk.KEY_A or keyval == Gdk.KEY_a:
                print('something')
            if keyval == Gdk.KEY_uparrow:
                print('something up')

    def get_word_score(self, word_guessed, current_word_to_find = ""):
        """
        Function to compare the guessed word to the real word to find
        """
        try:
            similarity = self.model.similarity(self.current_word_to_find, word_guessed)
            return float("{:.2f}".format(similarity * 100))
        except:
            return False

    def add_toast(self, text, timeout = 3) -> None:
        """
        Function to create and display a toast on the main Window
        """
        self.Main_ToastDisplay = Adw.Toast()
        self.Main_ToastDisplay.set_title(text)
        self.Main_ToastDisplay.set_timeout(timeout)
        self.Main_Toast.add_toast(self.Main_ToastDisplay)

    def show_about(self, action, param) -> None:
        """
        Function to create and show the AboutWindow
        """
        self.about = Adw.AboutDialog()
        self.about.set_application_icon('net.krafting.SemantiK')
        self.about.set_application_name("SemantiK")
        self.about.set_developers(["Krafting"])
        self.about.set_copyright("Copyright 2023 Krafting")
        self.about.set_license_type(Gtk.License.GPL_3_0)
        self.about.add_credit_section("Idée originale par", ["enigmatix"])
        self.about.add_credit_section("Données de", ["Jean-Philippe Fauconnier"])
        self.about.set_version("1.1.0")
        self.about.set_website("https://gitlab.com/Krafting/semantik-gtk/")
        self.about.set_issue_url("https://gitlab.com/Krafting/semantik-gtk/-/issues")
        self.about.present(self.get_active_window())

def main():
    app = SemantiK()
    app.run(sys.argv)

if __name__ == "__main__":
    main()
