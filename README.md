# SemantiK - Cémantix Clone GTK

## Screenshots

![Main App](./images/Screenshot-main-app-words.png)

## Roadmap

Here are the futur plan for this app, any help is appreciated!

- [ ] Translation
- [ ] Refactor and put stuff in multiple files for easier development
- [ ] Additionnal "Language Pack" downloadable via Flathub. 

## Requirements

- < Python3.11
- GTK/Adw
- Gensim

## How to run

`python3.11 SemantiK.py`

## How to build the flatpak

`flatpak-builder --user --install --force-clean build-dir net.krafting.SemantiK.yml && flatpak run net.krafting.SemantiK`

