# https://medium.com/nlplanet/text-similarity-with-the-next-generation-of-word-embeddings-in-gensim-466fdafa4423
# https://fauconnier.github.io/#data
# 

from gensim.models import KeyedVectors
from gensim.models import FastText




model = KeyedVectors.load_word2vec_format("frWac_non_lem_no_postag_no_phrase_200_cbow_cut100.bin", binary=True, unicode_errors="ignore")

sim = model.most_similar("intéressant")

print(sim)

word = "cerisier"

while True:
    word_guess = input("Mot : ")
    if not word_guess:
        continue
    try:
        similarity = model.similarity(word, word_guess)
        print(similarity)
    except:
        print("Le mot " + word_guess + " n'existe pas.")
